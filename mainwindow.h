#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    void InterfaceInit(void);
    void initSerialPort();
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_Open_pushButton_clicked();

    void on_SendpushButton_clicked();

    void readSerialDataSlot();

    void on_ClearReceButton_clicked();

    void on_ClearSendButton_clicked();

private:
    Ui::MainWindow *ui;
   // QSerialPort *MySerial;
    QSerialPort MySerial;//创建一个可操作的串口
    QTimer *MyTimer;//定义一个定时器
};

#endif // MAINWINDOW_H
