#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPortInfo>
#include <QByteArray>
#include <QString>
#include <QTextEdit>
#include <QMessageBox>
#include <QCheckBox>
#include <QDebug>


// 参考链接: https://blog.csdn.net/lmhuanying1012/article/details/78747737

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("火狐串口");//设置应用程序名称

    setMaximumSize(619,370);
    setMinimumSize(619,370);//设置应用程序固定大小
    //InterfaceInit();//初始化界面
    initSerialPort();
}

MainWindow::~MainWindow()//
{
    delete ui;
}



//串口初始化
void MainWindow::initSerialPort()
{
    //使用foreach获取有效的串口信息
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        //这里相当于自动识别串口号之后添加到了cmb，如果要手动选择可以用下面列表的方式添加进去
        MySerial.setPort(info);
        if(MySerial.open(QIODevice::ReadWrite))
        {
            //将串口号添加到cmb
            ui->COM_comboBox->addItem(info.portName());
            //关闭串口等待人为(打开串口按钮)打开
            MySerial.close();
        }
    }

    QStringList baudList;//波特率
    QStringList parityList;//校验位
    QStringList dataBitsList;//数据位
    QStringList stopBitsList;//停止位

    baudList<<"1200"<<"2400"<<"4800"<<"9600"<<"38400"<<"115200";

    ui->Bound_comboBox->addItems(baudList);//波特率
    ui->Bound_comboBox->setCurrentIndex(3);//从0开始索引 9600

    parityList<<"无"<<"奇"<<"偶";

    ui->CheckBit_comboBox->addItems(parityList);//校验位
    ui->CheckBit_comboBox->setCurrentIndex(0);

    dataBitsList<<"5"<<"6"<<"7"<<"8";
    ui->DataBit_comboBox->addItems(dataBitsList);//数据位
    ui->DataBit_comboBox->setCurrentIndex(3);

    stopBitsList<<"1";
    stopBitsList<<"1.5";
    stopBitsList<<"2";

    ui->StopBit_comboBox->addItems(stopBitsList);//停止位
    ui->StopBit_comboBox->setCurrentIndex(0);
}

//初始化信息
void MainWindow::InterfaceInit()
{

}


//打开串口按钮
void MainWindow::on_Open_pushButton_clicked()
{
    MySerial.setPortName(ui->COM_comboBox->currentText());//设置串口号;也就是说打开的是当前显示的串口
    if(ui->Open_pushButton->text()=="打开串口")//打开串口
    {
        if(MySerial.open(QIODevice::ReadWrite))//读写方式打开,成功后设置串口
        {
            MySerial.setBaudRate(ui->Bound_comboBox->currentText().toInt());//设置波特率
            MySerial.setDataBits(QSerialPort::Data8);//设置数据位
            MySerial.setParity(QSerialPort::NoParity);//设置校验位
            MySerial.setFlowControl(QSerialPort::NoFlowControl);//设置流控制
            MySerial.setStopBits(QSerialPort::OneStop);//设置停止位
            ui->Open_pushButton->setText("关闭串口");

            //使用定时器循环读取,接收的数据
            MyTimer = new QTimer(this);
            connect(MyTimer, SIGNAL(timeout()), this, SLOT(readSerialDataSlot()));//连接定时器时间到的槽函数
            MyTimer->start(100);//100ms
        }
        else//串口打开失败
        {
            ui->Open_pushButton->setText("打开失败");
            QMessageBox::about(NULL, "提示", "要打开串口哦！");
            return ;
        }
    }
    else if(ui->Open_pushButton->text()=="关闭串口")//关闭串口
    {
        ui->Open_pushButton->setText("打开串口");
        MyTimer->stop();//关定时器
        MySerial.close();//关串口
    }
}

//定时器时间到后执行的槽函数-读取串口数据槽函数 并显示在接收区
void MainWindow::readSerialDataSlot()
{
    QByteArray readData = MySerial.readAll();//读取串口数据

    if(readData != NULL)//将读到的数据显示到数据接收区
    {
        if(ui->DisplayCheckBox->isChecked()==true)//选中HEX显示
        {
            readData=readData.toHex();//转为HEX
            ui->ReceTextBrowser->append(readData);
        }
        else//未选中HEX显示
        {
            ui->ReceTextBrowser->append(readData);
        }
        readData.clear();//清除接收缓存
    }
}

//点击发送按钮槽函数-数据发送
void MainWindow::on_SendpushButton_clicked()
{
    QString SendData;
    QByteArray SendHex;//HEX发送
    if(ui->Open_pushButton->text()=="关闭串口")//打开串口才可以发送数据
    {
        if(!ui->SendTextBrowser->toPlainText().isEmpty())//发送区不为空
        {
            SendData = ui->SendTextBrowser->toPlainText();//获取发送区的数据
            if(ui->SendCheckBox->isChecked()==true)//SendCheckBox被选中HEX发送
            {
                SendHex.append(SendData).toHex();//转HEX存储
               // qDebug()<<SendHex<<endl;
            }
            else//没有选中HEX发送
            {
                SendHex.append(SendData);
                //qDebug()<<SendHex<<endl;
            }
            MySerial.write(SendHex,SendHex.length());//写入缓冲区
        }
        else//发送区为空
        {
            QMessageBox::about(NULL, "提示", "没有数据哦");
        }
    }
    else//串口未打开
    {
        QMessageBox::about(NULL, "提示", "要打开串口哦!");
    }
}

//清除接收按钮-处理接收区清空槽函数
void MainWindow::on_ClearReceButton_clicked()
{
    ui->ReceTextBrowser->clear();//清除接收区
}

//清除发送区按钮-处理发送区清空槽函数
void MainWindow::on_ClearSendButton_clicked()
{
    ui->SendTextBrowser->clear();//清除发送区
}
